<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cantantes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantantes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'nombre',
            'apellidos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>