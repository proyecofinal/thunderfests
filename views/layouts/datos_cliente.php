<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->nombre." " .$model->apellidos;
\yii\web\YiiAsset::register($this);
?>

<div class="clientes-view">
    <h2>Datos de <?= $model->nombre." " . $model->apellidos?></h2>

    <p><strong>DNI: </strong><?= $model->dni ?></p>
    <p><strong>Nombre: </strong><?= $model->nombre ?></p>
    <p><strong>Apellidos: </strong><?= $model->apellidos ?></p>
    <p><strong>Edad: </strong><?= $model->edad ?></p>
    <p><strong>Fecha de Nacimiento: </strong><?= $model->fecha_nacimiento ?></p>
    
    

</div>