<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cantan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantan-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_cantantes',
            'cod_conciertos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    


</div>