<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '@web/imagenes/logo.png']); ?>

        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            if (Yii::$app->user->isGuest) {
                NavBar::begin([
                    'brandLabel' => Html::img('@web/imagenes/logo.png', ['alt' => Yii::$app->name, 'style' => 'margin-top: -10px;']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top opc',
                        'style' => 'padding: 8px 8px;'
                    ],
                ]);
            } else {
                NavBar::begin([
                    'brandLabel' => Html::img('@web/imagenes/logo.png', ['alt' => Yii::$app->name, 'style' => 'margin-top: -10px;']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top opc',
                        'style' => 'padding: 8px 8px;'
                    ],
                ]);
            }
            if (Yii::$app->user->isGuest) {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-left'],
                    'items' => [
                        ['label' => 'Inicio', 'url' => ['/site/index'], 'options' => ['class' => 'opc']],
                        ['label' => 'Festivales', 'url' => ['/festivales/fests'], 'options' => ['class' => 'opc']],
                        ['label' => 'Entradas', 
                            'items' => [
                                ['label' => 'Comprar Entradas', 'url' => ['/entradas/ent'], 'linkOptions' => ['class' => 'redes']],
                                ['label' => 'Cambio de Nombre', 'url' => ['/compran/nombre'], 'linkOptions' => ['class' => 'redes']],
                            ], 'options' => ['class' => 'opc']],
                        ['label' => 'Condiciones', 'url' => ['/site/about'], 'options' => ['class' => 'opc']],
                        ['label' => 'Redes Sociales',
                            'items' => [
                                '<li>
                            <a class="redes" href="https://www.instagram.com/?hl=es" tabindex="-1" target="_blank">
                                Instagram
                                <i style="margin-left: 5px;" class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a class="redes" href="https://twitter.com/?lang=es" tabindex="-1" target="_blank">
                                Twitter
                                <i style="margin-left: 5px;" class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a class="redes" href="https://www.facebook.com/" tabindex="-1" target="_blank">
                                Facebook
                                <i style="margin-left: 5px;" class="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a class="redes" href="https://www.facebook.com/" tabindex="-1" target="_blank">
                                Youtube
                                <i style="margin-left: 5px;" class="fab fa-youtube"></i>
                            </a>
                        </li>'
                        
                            ], 'options' => ['class' => 'opc']
                        ],
                        ['label' => 'Contacto', 'url' => ['/site/contact'], 'options' => ['class' => 'opc']],
                        ['label' => 'Registro', 'url' => ['/clientes/create'], 'options' => ['class' => 'opc']],
                    ],
                ]);
            }else {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-left'],
                    'items' => [
                        ['label' => 'Administración', 'url' => ['/site/index']],
                        ['label' => 'Gestión', 
                            'items' => [
                                ['label' => 'Cantantes', 'url' => ['/cantantes/filter'], 'linkOptions' => ['class' => 'redes']],
                                ['label' => 'Conciertos', 'url' => ['/conciertos/filter'], 'linkOptions' => ['class' => 'redes']],
                                ['label' => 'Cantan', 'url' => ['/cantan/filter'], 'linkOptions' => ['class' => 'redes']],
                                ['label' => 'Instrumentos', 'url' => ['/instrumentos/filter'], 'linkOptions' => ['class' => 'redes']],
                            ], 'options' => ['class' => 'opc']],
                        ['label' => 'Log Out',  'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
                            
                        
                    ]
                ]);
            
            }
            NavBar::end();
            ?>
            <?= Alert::widget() ?>
            <?= $content ?>
                   
        
            
                
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span class="textfooter text-uppercase">&copy; <?=Yii::$app->name?> | </span>  
                        <?= Html::a('Condiciones | ', ['site/about']) ?> 
                        <?= Html::a('Contacto | ', ['site/contact']) ?> 
                        <a><?= Yii::powered() ?></a>
                    </div>
                </div>

            </div>
        </footer>

        <?php $this->endBody() ?>
        <script src="https://kit.fontawesome.com/a75ed5c33d.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

    </body>
</html>
<?php $this->endPage() ?>
