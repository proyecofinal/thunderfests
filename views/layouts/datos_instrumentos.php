<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Instrumentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_conciertos',
            'instrumentos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    


</div>
