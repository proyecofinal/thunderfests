<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = $model->cod_clientes;
$this->params['breadcrumbs'][] = ['label' => 'Comprans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="compran-view">

        <div class="col-md-12 entrada">
                <h2><?=$model->codFestivales->nombre?></h2>

                <p><strong><?=$model->codFestivales->fecha_inicio?> - <?=$model->codFestivales->fecha_fin?></strong></p></br>
                <p><strong>ABONO GENERAL - PRUEBA</strong></p>

                <div class="col-md-8 bg-condiciones">
                    <p><small>Nombre:</small></p></br>
                    <p class="text-uppercase"><?=$model->codClientes->nombre?></p>
                </div>
                <div class="col-md-4 bg-condiciones">
                    <p><small>Numero de Entrada:</small></p></br>
                    <p class="text-uppercase"><?=$model->codEntradas->numero_entrada?></p>
                </div>

            </div>
            <div>
                <small>
                    +Este abono te permite acceder al recinto de conciertos desde el <strong><?=$model->codFestivales->fecha_inicio?> hasta el <?=$model->codFestivales->fecha_fin?></strong>.</br>
                    +Este abono NO incluye el acceso a la zona de descanso. Es un ticket independiente que deberas comprar más adelante.</br>
                    </br>
                    La organización advierte que la compra de abonos fuera de los canales oficiales supone un riego para el asistente, pudiendo estar adquiriendo entradas duplicadas 
                    o falsas con las que se denegaria el acceso al festival. La organización realizará validación electronica del 100% de las entradas, negando el acceso a toda aquella 
                    entrada que ya haya sido previamente validada o adquirida fuera de los canales del festival <strong><?=$model->codFestivales->nombre?></strong>. Será necesario que los datos del DNI del portador
                    de la entrada coincidan con los que aparecen en la entrada para poder acceder al festival.
                </small>
                
            </div>
        </div>
    </div>
</div>