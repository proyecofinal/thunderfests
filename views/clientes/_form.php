<?php

use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="clientes-form">

            <h1><?=$titulo?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'edad')->textInput() ?>

            <?= $form->field($model, 'fecha_nacimiento')->widget(DateControl::classname(), [
                    'type' => 'date',
                    'ajaxConversion' => true,
                    'autoWidget' => true,
                    'widgetClass' => '',
                    'displayFormat' => 'php:d-m-Y',
                    'saveFormat' => 'php:Y-m-d',
                    'displayTimezone' => 'Europe/Brussels',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'php:d-m-Y'
                        ]
                    ],
                    'language' => 'es'
                ]);
            ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>