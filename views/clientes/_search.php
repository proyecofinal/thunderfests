<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\ClientesSearch */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Filtro de Clientes';
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="clientes-search">

            <h1>Filtro de Clientes</h1>

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="col-md-6">
                <?= $form->field($model, 'dni') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'nombre') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'apellidos') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'edad') ?>
            </div>
            <!--<div class="col-md-6"> No es necesario ya que existe el campo edad
                <?= $form->field($model, 'fecha_nacimiento')->widget(DateControl::classname(), [
                    'type' => 'date',
                    'ajaxConversion' => true,
                    'autoWidget' => true,
                    'widgetClass' => '',
                    'displayFormat' => 'php:d-m-Y',
                    'saveFormat' => 'php:Y-m-d',
                    'displayTimezone' => 'Europe/Brussels',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'php:d-m-Y'
                        ]
                    ],
                    'language' => 'es'
                ]); ?>
            </div>-->
            

            <div class="form-group text-center">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-warning']) ?>
                <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>