<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
if (Yii::$app->user->isGuest) {
  $this->title = 'Cambiar datos';
} else {
  $this->title = 'Cliente: ' . $model->nombre . " " . $model->apellidos;
}
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clientes-update">

  <?php 
    if (Yii::$app->user->isGuest) {
      $titulo =  "Cambiar mis Datos";
    } else {
      $titulo = "Actualizar datos de: " . Html::encode($model->nombre) . " " . Html::encode($model->apellidos);
    }?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
        
    ]) ?>

</div>
