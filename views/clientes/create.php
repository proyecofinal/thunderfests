<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
if (Yii::$app->user->isGuest) {
    $this->title = 'Registro';
} else {
    $this->title = 'Añadir Cliente';
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientes-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
