<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->nombre . " " . $model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="clientes-view">

            <?php if (Yii::$app->user->isGuest) { ?>
                <h1>Información de Cliente</h1>
                <p>
                    Le mostramos su información como cliente, 
                    si desea cambiar algun dato que le mostramos a continuación pulse
                    <?= Html::a('aquí', ['update', 'id' => $model->cod]) ?>
                </p>

            <?php } else { ?>
                <h1>Información del Cliente: <?= Html::encode($this->title) ?></h1>
                <p>
                    <?= Html::a('Actualización', ['update', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Borrar', ['delete', 'id' => $model->cod], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas borrar este registro?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
            <?php } ?>


            <?= $plantilla = DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'dni',
                    'nombre',
                    'apellidos',
                    'edad',
                    [
                        'label' => 'Fecha Nacimiento',
                        'value' => $model->fecha_nacimiento,
                        'format' => ['date', 'php:d-m-Y'],

                    ],

                ],
            ]) ?>

            <?php if (Yii::$app->user->isGuest) { ?>
                <strong>(Recomendamos descargar los datos, algunos se podran pedir mas adelante)</strong></br>

                <?= Html::a('Imprimir Datos', ['imprimir', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>
            <?php } ?>
        </div>
    </div>
</div>