<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="clientes-index">

            <?php
            $gridColumns = [
                'dni',
                'nombre',
                'apellidos',
                'edad',
                'fecha_nacimiento',
            ];

            // Renders a export dropdown menu

            ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                
                'columns' => [
                    'dni',
                    'nombre',
                    'apellidos',
                    'edad',
                    /*[
                        'attribute' => 'fecha_nacimiento',
                        'value' => 'fecha_nacimiento',
                        'format' => ['date', 'php:d-m-Y'],
                        'filter' => DatePicker::widget([
                            'options' => ['placeholder' => '0000-00-00'],
                            'model' => $searchModel,
                            'attribute' => 'fecha_nacimiento',
                            'language' => 'es',
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]


                        ]),
                    ],*/


                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'toolbar' => [
                    [
                        'content' => ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $gridColumns,
                            'clearBuffers' => true, //optional
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
                            ],
                            'filename' => 'Datos_Clientes' . ' ' . date('d-m-y')
                        ]),

                    ],



                ],
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="fas fa-user"></i> ' . $this->title . '</h3>',
                    'type' => 'warning',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                        'type' => 'button',
                        'title' => 'Crear cliente',
                        'class' => 'btn btn-warning'
                    ]),
                    

                ],

            ]); ?>

        </div>
    </div>
</div>