<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Festivales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="site-fests">
            <div class="col-md-12" style="padding-top: 10px; padding-bottom: 10px;">
                <div class="col-md-5" style="margin-bottom: 10px;">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="text-uppercase">Medusa</h3>
                            <p>El Medusa Sunbeach Festival es un festival de música electrónica dirigido principalmente
                                hacia un público joven de 16 años hasta la sepultura. Se celebra desde 2014 cada año <strong>en
                                    la playa de Cullera, municipio de Valencia, Comunidad Valenciana.</strong>
                            <p>
                            <div class="text-center">
                                <?= Html::a('Más Información', [''], ['class' => 'btn btn-warning', 'disabled' => 'true']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="vl"></div>
                </div>
                <div class="col-md-5">
                    <?= Html::img('../../imagenes/fondo.jpg', ['alt' => 'some', 'class' => 'img-responsive']) ?>
                </div>
            </div>

            <div class="col-md-12 d-flex d-flex-reverse " style="padding-top: 10px; padding-bottom: 10px;">
                <div class="col-md-5">
                    <?= Html::img('../../imagenes/fondo.jpg', ['alt' => 'some', 'class' => 'img-responsive']) ?>
                </div>
                <div class="col-md-2">
                    <div class="vl" style="margin-left: 150px;"></div>
                </div>
                <div class="col-md-5" style="margin-bottom: 10px;">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="text-uppercase">Arenal</h3>
                            <p>El festival Arenal Sound es un festival de 
                                música independiente que se celebra en la playa El Arenal, 
                                en la localidad de Burriana (provincia de Castellón, España), 
                                durante la primera semana de agosto desde 2010. Se caracteriza 
                                por su gran afluencia de jóvenes y se diferencia de otros festivales por su cercanía a la playa al tener uno de sus escenarios sobre la misma arena. 
                            <p>
                            <div class="text-center">
                                <?= Html::a('Más Información', [''], ['class' => 'btn btn-warning', 'disabled' => 'true']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 10px; padding-bottom: 10px;">
                <div class="col-md-5" style="margin-bottom: 10px;">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="text-uppercase">Riverland</h3>
                            <p>Riverland es un festival de música indie, pop, rock que tendrá lugar los días 14, 15, 16 y 17 de julio de 2021 en Arriondas – Cangas de Onís (Asturias), coincidiendo con el Descenso Internacional del Sella. Un festival en un marco único, rodeado de naturaleza y a la orilla del río más emblemático de Asturias.
                                    La Fiesta de les Piragües tendrá un nuevo festival de música.
                            <p>
                            <div class="text-center">
                                <?= Html::a('Más Información', [''], ['class' => 'btn btn-warning', 'disabled' => 'true']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="vl"></div>
                </div>
                <div class="col-md-5">
                    <?= Html::img('../../imagenes/fondo.jpg', ['alt' => 'some', 'class' => 'img-responsive']) ?>
                </div>
            </div>
            <div class="col-md-12 d-flex d-flex-reverse" style="padding-top: 10px; padding-bottom: 10px;">
                <div class="col-md-5">
                    <?= Html::img('../../imagenes/fondo.jpg', ['alt' => 'some', 'class' => 'img-responsive']) ?>
                </div>
                <div class="col-md-2">
                    <div class="vl" style="margin-left: 150px;"></div>
                </div>
                <div class="col-md-5" style="margin-bottom: 10px;">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="text-uppercase">Sunblast</h3>
                            <p>El mayor festival de Canarias! en SUNBLAST 360 FESTIVAL | Costa Adeje | Tenerife
                            <p>
                            <div class="text-center">
                                <?= Html::a('Más Información', [''], ['class' => 'btn btn-warning', 'disabled' => 'true']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>