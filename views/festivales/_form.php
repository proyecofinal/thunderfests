<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Festivales */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="festivales-form">

            <h1><?= $titulo ?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'fecha_inicio')->widget(DateControl::classname(), [
                'type' => 'date',
                'ajaxConversion' => true,
                'autoWidget' => true,
                'widgetClass' => '',
                'displayFormat' => 'php:d-m-Y',
                'saveFormat' => 'php:Y-m-d',
                'saveTimezone' => 'UTC',
                'displayTimezone' => 'Europe/Brussels',
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'php:d-m-Y'
                    ]
                ],
                'language' => 'es'
            ]);  ?>

            <?= $form->field($model, 'fecha_fin')->widget(DateControl::classname(), [
                'type' => 'date',
                'ajaxConversion' => true,
                'autoWidget' => true,
                'widgetClass' => '',
                'displayFormat' => 'php:d-m-Y',
                'saveFormat' => 'php:Y-m-d',
                'saveTimezone' => 'UTC',
                'displayTimezone' => 'Europe/Brussels',
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'php:d-m-Y'
                    ]
                ],
                'language' => 'es'
            ]);  ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>