<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Festivales */

$this->title = 'Añadir Festival';
$this->params['breadcrumbs'][] = ['label' => 'Festivales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivales-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
