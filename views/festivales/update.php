<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Festivales */

$this->title = 'Festival: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Festivales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="festivales-update">

    <?php $titulo =  "Actualizar Festival: " . Html::encode($model->nombre) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
