<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Festivales */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Festivales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="festivales-view">

            <h1>Información del Festival: <?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Actualización', ['update', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Borrar', ['delete', 'id' => $model->cod], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estas seguro que quieres borrar este festival?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'cod',
                    'nombre',
                    [
                        'label' => 'Fecha de Comiezo',
                        'value' => $model->fecha_inicio,
                        'format' => ['date', 'php:d-m-Y'],

                    ],
                    [
                        'label' => 'Fecha de Finalización',
                        'value' => $model->fecha_fin,
                        'format' => ['date', 'php:d-m-Y'],

                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>