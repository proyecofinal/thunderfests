<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\datecontrol\DateControl;
use kartik\date\DatePicker;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Festivales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="festivales-index">

            <?php
            $gridColumns = [
                'nombre',
                'fecha_inicio',
                'fecha_fin',
            ];

            // Renders a export dropdown menu

            ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                
                'columns' => [
                    'nombre',
                    [
                        'attribute' => 'fecha_inicio',
                        'value' => 'fecha_inicio',
                        'format' => ['date', 'php:d-m-Y'],
                        
                    ],
                    [
                        'attribute' => 'fecha_fin',
                        'value' => 'fecha_fin',
                        'format' => ['date', 'php:d-m-Y'],
                        
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'toolbar' => [
                    [
                        'content' => ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $gridColumns,
                            'clearBuffers' => true, //optional
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
                            ],
                            'filename' => 'Datos_Clientes' . ' ' . date('d-m-y')
                        ]),

                    ],




                ],
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="fas fa-umbrella-beach"></i> ' . $this->title . '</h3>',
                    'type' => 'warning',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                        'type' => 'button',
                        'title' => 'Añadir Festival',
                        'class' => 'btn btn-warning'
                    ]),
                    

                ],

            ]); ?>
        </div>
    </div>
</div>