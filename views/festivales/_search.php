<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FestivalesSearch */
/* @var $form yii\widgets\ActiveForm */
// $this->title = 'Filtro Festivales';
$this->title = 'Filtro de Festivales';
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="festivales-search">

            <h1>Filtro de Festivales</h1>

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
            <div class="col-md-4">
                <?= $form->field($model, 'nombre') ?>
            </div>
            <div class="col-md-8 input-group drp-container">
                <label> Rango de Fechas </label>
                <?php
                echo DateRangePicker::widget([
                    'model' => $model,
                    'attribute' => 'fecha_inicio',
                    'useWithAddon' => true,
                    'convertFormat' => true,
                    'startAttribute' => 'fecha_inicio',
                    'endAttribute' => 'fecha_fin',
                    'pluginOptions' => [
                        'locale' => ['format' => 'Y-m-d'],
                        'opens'=>'left',
                       
                    ],
                    
                    
                ])?>
            </div>
            </br>
            <div class="form-group text-center">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-warning']) ?>
                <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>