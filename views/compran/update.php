<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = 'Actualizar Compra';
$this->params['breadcrumbs'][] = ['label' => 'Comprans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_clientes, 'url' => ['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compran-update">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
