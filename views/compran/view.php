<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = "Entrada de " . $model->codClientes->nombre ;
$this->params['breadcrumbs'][] = ['label' => 'Compra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="compran-view">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php if (Yii::$app->user->isGuest) { ?>
                <p>
                    Este código se le pedira si desea realizar un cambio de nombre de nombre
                </p>
            <?php } else { ?>
                <p>
                    <?= Html::a('Actualización', ['update', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Borrar', ['delete', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas borrar este registro?',
                            'method' => 'post',
                        ],
                    ]) ?>
                <p>
            <?php }  ?>
            

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label' => 'Nombre del Cliente',
                        'value' => $model->codClientes->nombre,

                    ],
                    [
                        'label' => 'Numero de Entrada',
                        'value' => $model->codEntradas->numero_entrada,

                    ],
                    [
                        'label' => 'Nombre del  Festival',
                        'value' => $model->codFestivales->nombre,

                    ],
                ],
            ]) ?>
            <?php if (Yii::$app->user->isGuest) { ?>
                <?= Html::a('Imprimir Entrada', ['imprimir', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales], ['class' => 'btn btn-warning']) ?>
            <?php } ?>
            
        </div>
    </div>
</div>