<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Festivales;
use app\models\Entradas;
use app\models\Clientes;


/* @var $this yii\web\View */
/* @var $model app\models\Compran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="compran-form">

            <h1><?=$titulo?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'cod_clientes')->dropDownList(
                    ArrayHelper::map(Clientes::find()->all(),'cod','dni'),
                        ['prompt' => 'Selecciona un festival'])->label('Dni del Nuevo Cliente') ?>
            
            <?= $form->field($model, 'cod_entradas')->dropDownList(
                    ArrayHelper::map(Entradas::find()->all(),'cod','numero_entrada'),
                        ['prompt' => 'Selecciona un festival', 'disabled' => ($model->isNewRecord) ? false : true])->label('Numero de la entrada') ?>
            
            <?= $form->field($model, 'cod_festivales')->dropDownList(
                    ArrayHelper::map(Festivales::find()->all(),'cod','nombre'),
                        ['prompt' => 'Selecciona un festival', 'disabled' => ($model->isNewRecord) ? false : true])->label('Nombre del Festival') ?>

            


            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>