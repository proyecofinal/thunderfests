<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = 'Comprar Entradas';
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compran-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
        
    ]) ?>

</div>
