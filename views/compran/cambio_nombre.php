<?php

use app\models\Clientes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Compran;
//$model = new Compran;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */
/* @var $form yii\widgets\ActiveForm */
//$cod_clientes = "";
$this->title = 'Cambio Nombre';
$this->params['breadcrumbs'][] = $this->title;
$model = new Clientes();
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="compran-form">

            <h1>Cambiar Nombre</h1>

            <?php $form = ActiveForm::begin([
                'action' => ['compran/cambio'],
                
                
                
            ]); ?>

            <div class="cod-md-12">
                <?= $form->field($model, 'dni') ?>
            </div>
            
            <div class="form-group">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>  
    </div>
</div>