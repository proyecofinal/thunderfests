<?php

use app\models\Clientes;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = $model->codClientes->nombre . " " . $model->codClientes->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Comprans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$clientes = new Clientes();
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="compran-view">

            <h1><?= Html::encode($this->title) ?></h1>
            
            <?= DetailView::widget([
                'model' => $model,
                

                'attributes' => [
                    [                                                  
                        'label' => 'Nombre del Cliente',
                        'value' => $model->codClientes->nombre,
                        
                    ],
                    [                                                  
                        'label' => 'Numero de Entrada',
                        'value' => $model->codEntradas->numero_entrada,
                        
                    ],
                    [                                                  
                        'label' => 'Nombre del  Festival',
                        'value' => $model->codFestivales->nombre,
                        
                    ],
                ],
            ]) ?>
            
            <div class="form-group">
                <?= Html::a('Cambiar', ['actualizar', 'cod_clientes' => $cliente, 'cod_entradas' => $entrada, 'cod_festivales' => $festival ], ['class' => 'btn btn-warning']) ?>
            </div>
        </div>
    </div>
</div>