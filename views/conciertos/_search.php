<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ConciertosSearch */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Filtro de Conciertos';
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="conciertos-search">

            <h1>Filtro de Conciertos</h1>

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="col-md-6">
                <?= $form->field($model, 'fecha')->widget(DateControl::classname(), [
                    'type' => 'date',
                    'ajaxConversion' => true,
                    'autoWidget' => true,
                    'widgetClass' => '',
                    'displayFormat' => 'php:d-m-Y',
                    'saveFormat' => 'php:Y-m-d',
                    'displayTimezone' => 'Europe/Brussels',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'php:d-m-Y'
                        ]
                    ],
                    'language' => 'es'
                ]); ?>
            </div>
            
            <div class="col-md-6">
                <?= $form->field($model, 'Nombre_Festivales') ?>
            </div> 
            <div class="form-group text-center">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-warning']) ?>
                <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>