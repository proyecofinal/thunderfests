<?php

use app\models\Conciertos;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conciertos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="entradas-index">

        <?php
            $gridColumns = [
                'fecha',
                'hora_inicio',
                'hora_fin',
                'Festival', 
                
            ];

            // Renders a export dropdown menu

            ?>

            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                
                'columns' => [
                    [
                        'attribute' => 'fecha',
                        'value' => 'fecha',
                        'format' => ['date', 'php:d-m-Y'],
                        
                    ],
                    'hora_inicio'
                    ,
                    'hora_fin'
                    ,
                    [
                        'attribute' => 'Festival',
                        'value' => 'codFestivales.nombre',
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'toolbar' => [
                    [
                        'content' => ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $gridColumns,
                            'clearBuffers' => true, //optional
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
                            ],
                            'filename' => 'Datos_Clientes' . ' ' . date('d-m-y')
                        ]),

                    ],



                ],
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="fas fa-music"></i> ' . $this->title . '</h3>',
                    'type' => 'warning',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                        'type' => 'button',
                        'title' => 'Añadir Concierto',
                        'class' => 'btn btn-warning'
                    ]),
                    

                ],

            ]); ?>
        </div>
    </div>
</div>

