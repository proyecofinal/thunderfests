<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conciertos */

$this->title = 'Crear Nuevo Concierto';
$this->params['breadcrumbs'][] = ['label' => 'Conciertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conciertos-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'nombre' => $nombre,
        'titulo' => $titulo,
    ]) ?>

</div>
