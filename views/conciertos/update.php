<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conciertos */

$this->title = 'Concierto: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Conciertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conciertos-update">

    <?php $titulo =  "Actualizar Concierto: " . Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'nombre' => $nombre,
        'titulo' => $titulo,
    ]) ?>

</div>
