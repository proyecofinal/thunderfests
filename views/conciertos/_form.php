<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use app\models\Festivales;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Conciertos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="conciertos-form">
        
            <h1><?= $titulo ?></h1>
            
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'fecha')->widget(DateControl::classname(), [
                'type' => 'date',
                'ajaxConversion' => true,
                'autoWidget' => true,
                'widgetClass' => '',
                'displayFormat' => 'php:d-m-Y',
                'saveFormat' => 'php:Y-m-d',
                'displayTimezone' => 'Europe/Brussels',
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'php:d-m-Y'
                    ]
                ],
                'language' => 'es'
            ]); ?>

            <?= $form->field($model, 'hora_inicio')->textInput() ?>

            <?= $form->field($model, 'hora_fin')->textInput() ?>

            <?= $form->field($model, 'cod_festivales')->dropDownList(
                    ArrayHelper::map(Festivales::find()->all(),'cod','nombre'),
                        ['prompt' => 'Selecciona un festival'])->label('Nombre del Festival') ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>