<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cantantes;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="cantan-form">

            <h1><?=$titulo?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'cod_cantantes')->dropDownList(
                    ArrayHelper::map(Cantantes::find()->all(),'cod','nombre'),
                        ['prompt' => 'Selecciona un Cantante'])->label('Nombre del Cantante') ?>

            <?= $form->field($model, 'cod_conciertos')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>