<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = 'Cantan: ' . $model->cod_cantantes;
$this->params['breadcrumbs'][] = ['label' => 'Cantans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_cantantes, 'url' => ['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cantan-update">

    <?php $titulo =  "Actualizar Cantan: " . Html::encode($model->codCantantes->nombre) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
