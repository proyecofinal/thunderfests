<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cantan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="entradas-index">

        <?php
            $gridColumns = [
                'Cantante',
                'cod_conciertos',
            ];

            // Renders a export dropdown menu

            ?>

            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                
                'columns' => [
                    [
                        'attribute' => 'Cantante',
                        'value' => 'codCantantes.nombre',
                    ],
                    'cod_conciertos',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'toolbar' => [
                    [
                        'content' => ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $gridColumns,
                            'clearBuffers' => true, //optional
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_EXCEL_X => false,              
                            ],
                            'filename' => 'Datos_Clientes' . ' ' . date('d-m-y')
                        ]),

                    ],



                ],
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="fas fa-microphone-alt"></i> ' . $this->title . '</h3>',
                    'type' => 'warning',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                        'type' => 'button',
                        'title' => 'Añadir Reparto',
                        'class' => 'btn btn-warning'
                    ]),
                    

                ],

            ]); ?>
        </div>
    </div>
</div>
