<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = "Codigo: " . $model->cod_cantantes;
$this->params['breadcrumbs'][] = ['label' => 'Cantan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="cantan-view">

            <h1>Cantante: <?= Html::encode($model->codCantantes->nombre) . " " . Html::encode($model->codCantantes->apellidos) ?></h1>

            <p>
                <?= Html::a('Actualización', ['update', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Borrar', ['delete', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas borrar este registro?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label' => 'Nombre del Cantante',
                        'value' => $model->codCantantes->nombre,

                    ],
                    'cod_conciertos',
                ],
            ]) ?>
        </div>
    </div>
</div>