<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = 'Crear nuevo Cantan';
$this->params['breadcrumbs'][] = ['label' => 'Cantans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantan-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
