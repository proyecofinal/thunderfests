<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid" style="padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="instrumentos-form">

            <h1><?= $titulo ?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'cod_conciertos')->textInput() ?>

            <?= $form->field($model, 'instrumentos')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>