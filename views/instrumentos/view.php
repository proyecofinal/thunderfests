<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */

$this->title = "Concierto: " . $model->cod_conciertos;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="instrumentos-view">

            <h1><?= "Instrumento: " . Html::encode($model->instrumentos) ?></h1>

            <p>
                <?= Html::a('Actualización', ['update', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Borrar', ['delete', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas borrar este registro?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'cod_conciertos',
                    'instrumentos',
                ],
            ]) ?>
        </div>
    </div>
</div>
