<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */

$this->title = 'Actualizar Instrumento: ' . $model->instrumentos;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_conciertos, 'url' => ['view', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instrumentos-update">

    <h1><?= $titulo = Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
