<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */

$this->title = 'Cantante: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Cantantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cantantes-update">

    <?php $titulo =  "Actualizar al Cantante: " . Html::encode($model->nombre) . " " . Html::encode($model->apellidos)?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
