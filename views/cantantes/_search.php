<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CantantesSearch */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Filtro de Cantantes';
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="cantantes-search">

            <h1>Filtro de Cantantes</h1>
        
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
                    
        <div class="col-md-6">
            <?= $form->field($model, 'nombre') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'apellidos') ?>
        </div>
        
            <div class="form-group text-center">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-warning']) ?>
                <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>