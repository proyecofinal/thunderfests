<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="cantantes-form">
        
            <h1><?=$titulo?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>