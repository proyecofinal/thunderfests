<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */

$this->title = "Cantante: " . $model->nombre . " " . $model->apellidos ;
$this->params['breadcrumbs'][] = ['label' => 'Cantantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="cantantes-view">

            <h1>Información del Cantante: <?= Html::encode($model->nombre), " " . Html::encode($model->apellidos)?></h1>

            <p>
                <?= Html::a('Actualización', ['update', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Borrar', ['delete', 'id' => $model->cod], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas borrar este registro?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'cod',
                    'nombre',
                    'apellidos',
                ],
            ]) ?>
        </div>
    </div>
</div>