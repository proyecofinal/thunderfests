<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */

$this->title = 'Crear un Cantante';
$this->params['breadcrumbs'][] = ['label' => 'Cantantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantantes-create">

    <?php $titulo =  Html::encode($this->title) ?>

    <?= $this->render('_form', [
        'model' => $model,
        'titulo' => $titulo,
        'nombre' => $nombre,
    ]) ?>

</div>
