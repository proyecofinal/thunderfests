<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Condiciones Generales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container">
        <div class="col-md-1"></div>
        <div class="site-about col-md-10 bg-condiciones">
            <h1 class="text-center text-uppercase"><?= Html::encode($this->title) ?></h1>
            <div class="row" style="margin-left: 5px; margin-right: 5px;">
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">1. </span>
                    La entrada para cada Festival, en cualquiera de sus formatos (papel, pulsera, etc) es válida únicamente para el día indicado. No se admiten cambios ni devoluciones. Cada entrada está identificada con un código “QR” único que se debe preservar para que no sea copiado. Evita exponerlo, así como subir fotos sin cubrirlo, ya que sólo podrá pasar una vez por nuestro control de acceso.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">2. </span>
                    ESTÁ RESERVADO EL DERECHO DE ADMISIÓN. El acceso sólo se permitirá con la entrada completa. Toda entrada rota, en mal estado, enmendada o con signos de falsificación autorizará a la organización a prohibir a su portador el acceso al recinto sin derecho a devolución del importe correspondiente. En el caso de pulseras deterioradas o que no puedan ponerse o quitarse por cualquier causa, siempre que conserven su código QR intacto, podrán cambiarse por una nueva en la puerta de acceso, con un coste de 5€.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">3. </span>
                    Es imprescindible presentar el DNI o cualquier otro documento oficial ORIGINAL que demuestre la mayoría de edad exigida. No se aceptarán fotocopias, denuncias por robo o extravío, ni documentos que contengan tachaduras o signos de manipulación.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">4. </span>
                    En protección de los derechos de imagen y propiedad intelectual, queda terminantemente prohibido filmar o grabar la totalidad o parte del Festival mediante vídeo, audio o por cualquier otro medio, así como el uso de cámaras fotográficas PROFESIONALES.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">5. </span>
                    Se prohíbe la entrada con botellas, latas, paraguas, palos de selfie , armas o cualquier objeto que la organización considere peligroso, así como a todas las personas que manifiesten conductas violentas e impropias o síntomas de estar bajo los efectos de bebidas alcohólicas y/o sustancias estupefacientes.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">6. </span>
                    Los vasos para el consumo de bebidas son de pago (1€). Lleva tu propio vaso de cualquier evento anterior de FARRA <strong>o tu moneda preparada</strong>, la empresa no está obligada a proporcionarte cambio si pagas con billetes de más de 20€.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">7. </span>
                    Es imprescindible para acceder al evento tener 18 años ya cumplidos el día de celebración del mismo. Dicho acceso está sujeto a un registro conforme a la Ley. A quienes no acrediten de forma inequívoca su edad o se nieguen a dicho registro, no se les permitirá la entrada al recinto. En ningún caso se podrá reclamar la devolución del importe de las entradas.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">8. </span>
                    El poseedor de una entrada reconoce que su imagen podrá aparecer en tomas de vídeo o fotográficas realizadas en el recinto por diferentes medios para su posterior difusión informativa y/o promocional, y acepta y presta su conformidad para dichos usos.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">9. </span>
                    La empresa organizadora del Festival se reserva el derecho de alterar o cambiar, en cualquier momento, sin previo aviso o condición, tanto el Recinto de celebración como el programa establecido, incluido el line up, por motivos justificados de ausencia de cualquiera de los artistas, debido a enfermedad sobrevenida, pérdida de documentación, vuelo u otro medio de transporte y cualquier otra causa objetiva que se considere. Dichos cambios no conllevan devolución del importe de las entradas.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">10. </span>
                    La organización no garantiza la autenticidad de una entrada si no ha sido adquirida a través de los canales oficiales de venta. La posesión de una entrada falsificada, además de no facilitar el acceso al recinto, generará las acciones legales pertinentes. NO COMPRES TU ENTRADA A CUALQUIER PERSONA DE LA CALLE.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">11. </span>
                    El poseedor de una entrada perderá sus derechos al salir del recinto, ya sea voluntariamente, por desalojo generalizado de la autoridad competente o por indicaciones del personal autorizado, por motivos de seguridad, desorden o mala conducta, responsabilizándose el portador de sus propias acciones y omisiones que causen lesiones a terceros o daños a bienes. En ningún caso se tendrá derecho a la devolución del importe de las entradas.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">12. </span>
                    La cancelación del evento por Orden y/o Recomendación de las autoridades competentes como consecuencia de fenómenos climáticos adversos o acontecimientos sobrevenidos de índole natural, epidemiológicos, cierre del espacio aéreo o cualesquiera otros que se consideren objetivamente peligrosos o supongan potencialmente riesgo para los asistentes y para la propia Organización, no conllevará la devolución del importe de las entradas.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">13. </span>
                    Si la fecha del evento variara por cualquier motivo, la entrada ya adquirida será automáticamente válida para la fecha definitiva. En este supuesto no se podrá reclamar la devolución del importe de la misma.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">14. </span>
                    La empresa Promotora GARANTIZA el reembolso del importe nominal de las entradas, si el evento fuera cancelado por causas imputables a la Empresa, estableciéndose para ello un plazo máximo razonable que sea suficiente para la organización y desarrollo de tales acciones en los puntos de venta. Los gastos que se hubieran podido ocasionar por transporte, desplazamientos, reservas de hoteles, apartamentos y cualesquiera otros que pudieran aducirse como derivados del evento cancelado no podrán ser reclamados a la empresa ni serán tenidos en cuenta.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">15. </span>
                    Está PROHIBIDA la reventa de entradas, aunque fuera para la utilización de la misma con fines promocionales o sociales, sin el consentimiento expreso y escrito de los promotores del Evento.
                </p>
                <p>
                    <span class="numeracion" style=" color: #ffda3c;">16. </span>
                    El portador de la entrada (pulsera) declara expresamente que ha leído, comprendido y, por tanto, acepta estas Condiciones y asimismo manifiesta que es totalmente consciente de los derechos y obligaciones que de ellas emanan.
                </p>
            </div>
        </div>
    </div>
</div>