<?php

/* @var $this yii\web\View */

use yii\helpers\Html;



$this->title = 'Thunder Festivals';
?>

<div class="container-fluid bg-inicio" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">

    <div class="container transparencia">
        <div class="site-index">
            <?php if (Yii::$app->user->isGuest) { ?>
                <div class="col-md-12">
                    <div class="container-card" style="margin-top: 10px; margin-bottom: 10px;">
                        <iframe width="95%" height="450px" src="https://www.youtube.com/embed/3oXLMw1EuXg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <h2 class="text-uppercase" style="color: #FFF; font-family:Arial, Helvetica, sans-serif;">Festivales</h2>
                    <div style="background-color: #FA9017; margin: 15px; padding: 5px;">
                        <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                            'events' => $events,
                        )); ?>
                    </div>

                    <h2 class="text-uppercase" style="color: #FFF; font-family:Arial, Helvetica, sans-serif;">Patrocinadores</h2>
                    <div class="container-card" style="margin-bottom: 10px;">
                        <div class="card">
                            <div class="face face1">
                                <div class="content">
                                    <img style="color: #FFF;" src="../../imagenes/card2.png">

                                </div>
                            </div>
                            <div class="face face2">
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="face face1">
                                <div class="content">
                                    <img src="../../imagenes/card3.png">

                                </div>
                            </div>
                            <div class="face face2">
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="face face1">
                                <div class="content">
                                    <img src="../../imagenes/card1.png">

                                </div>
                            </div>
                            <div class="face face2">
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum cumque minus iste veritatis provident at.</p>
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-12">
                    <div class="col-md-12 d-flex flex-baseline" style="margin-top: 10px; margin-bottom: 10px;">
                        <div class="col-md-5 text-center bg-transparente">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h2>Festivales Actuales</h2>
                                    <p>Nº de Festivales: <?= $festivales ?></p>
                                    <div>
                                        <?= Html::a('Administrar', ['/festivales/index'], ['class' => 'btn btn-warning']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mt"></div>
                        <div class="col-md-5 text-center bg-transparente">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h2>Registrados</h2>
                                    <p>Nº de Clientes: <?= $clientes ?></p>
                                    <div>
                                        <?= Html::a('Administrar', ['/clientes/filter'], ['class' => 'btn btn-warning']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 text-center bg-transparente" style="margin-top: 15px;">
                            <div class="thumbnail">
                                <div class="caption">
                                    <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">

                                                <div class="carousel-caption">
                                                    <h2>Aforo(Medusa)</h2>
                                                    <p> <?= $aforo['medusa'] ?> personas</p>
                                                    <?= Html::a('Administrar', ['/entradas/filter'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Aforo(Arenal)</h2>
                                                    <p> <?= $aforo['arenal'] ?> personas</p>
                                                    <?= Html::a('Administrar', ['/entradas/filter'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Aforo(Riverland)</h2>
                                                    <p> <?= $aforo['riverland'] ?> personas</p>
                                                    <?= Html::a('Administrar', ['/entradas/filter'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Aforo(Sunblast)</h2>
                                                    <p> <?= $aforo['sunblast'] ?> personas</p>
                                                    <?= Html::a('Administrar', ['/entradas/filter'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel1" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 text-center bg-transparente">
                            <div class="thumbnail">
                                <div class="caption">
                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">

                                                <div class="carousel-caption">
                                                    <h2>Entradas Vendidas(Medusa)</h2>
                                                    <p><?= round($procentaje['medusa'], 1) ?>%</p>
                                                    <?= Html::a('Administrar', ['/compran/index'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Entradas Vendidas(Arenal)</h2>
                                                    <p><?= round($procentaje['arenal'], 1) ?>%</p>
                                                    <?= Html::a('Administrar', ['/compran/index'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Entradas Vendidas(Riverland)</h2>
                                                    <p><?= round($procentaje['riverland'], 1) ?>%</p>
                                                    <?= Html::a('Administrar', ['/compran/index'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                            <div class="item">

                                                <div class="carousel-caption">
                                                    <h2>Entradas Vendidas(Sunblast)</h2>
                                                    <p><?= round($procentaje['sunblast'], 1) ?>%</p>
                                                    <?= Html::a('Administrar', ['/compran/index'], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 10px;']) ?>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="text-center bg-transparente" style="margin-top: 15px;">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h2>ADVERTENCIA</h2>
                                    <p>Ahora estas loggeado como Administrador.Cualquier cambio que realices tendrá impacto sobre la Base de Datos. Piénsalo bien antes de tocar nada. Los cambios son irreversibles</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
    </div>
<?php } ?>
</div>
</div>
</div>