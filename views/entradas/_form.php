<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Festivales;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="entradas-form">

            <h1><?= $titulo ?></h1>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'numero_entrada')->textInput() ?>

            <?= $form->field($model, 'cod_festivales')->dropDownList(
                    ArrayHelper::map(Festivales::find()->all(),'cod','nombre'),
                        ['prompt' => 'Selecciona un festival'])->label('Nombre del Festival') ?>

            <div class="form-group">
            <div class="form-group">
                <?php if (Yii::$app->user->isGuest) { ?>
                    <?= Html::submitButton('Registrarme', ['class' => 'btn btn-warning']) ?>

                <?php } else { ?>
                    <?= Html::submitButton($nombre, ['class' => 'btn btn-warning']) ?>
                <?php } ?>
            </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>