<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

$this->title = "Entrada " . $model->numero_entrada;
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="entradas-view">

            <h1>Número de Entrada: <?= Html::encode($model->numero_entrada) ?></h1>

            <p>
                <?= Html::a('Actualización', ['update', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Borrar', ['delete', 'id' => $model->cod], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas borrar este registro?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'cod',
                    'numero_entrada',
                    [
                        'label' => 'Nombre del  Festival',
                        'value' => $model->codFestivales0->nombre,

                    ],
                ],
            ]) ?>

        </div>
    </div>
</div>