<?php
/* @var $this yii\web\View */

use app\models\Entradas;
use app\models\Festivales;
use yii\helpers\Html;


$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid" style="margin-top: 65px; padding-top: 30px; padding-bottom: 30px;">
    <div class="container bg-condiciones">
        <div class="site-entradas">
            <div class="col-md-12">
                <h1>Festivales Disponibles</h1>
                <?php if ($procentaje['arenal'] != 100) { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Arenal</h2>
                                <p><?= round($procentaje['arenal'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Arenal</h2>
                                <p><?= round($procentaje['arenal'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/entradas/ent'], ['class' => 'btn btn-warning', 'disabled' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-md-2 mt"></div>
                <?php if ($procentaje['riverland'] != 100) { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Riverland</h2>
                                <p><?= round($procentaje['riverland'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Riverland</h2>
                                <p><?= round($procentaje['riverland'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/entradas/ent'], ['class' => 'btn btn-warning', 'disabled' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="col-md-12 mt">
                <?php if ($procentaje['sunblast'] != 100) { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Sunblast</h2>
                                <p><?= round($procentaje['sunblast'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Sunblast</h2>
                                <p><?= round($procentaje['sunblast'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/entradas/ent'], ['class' => 'btn btn-warning', 'disabled' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-md-2 mt"></div>
                <?php if ($procentaje['medusa'] != 100) { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Medusa</h2>
                                <p><?= round($procentaje['medusa'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-5 text-center bordes">
                        <div class="thumbnail">
                            <div class="caption">
                                <h2>Medusa</h2>
                                <p><?= round($procentaje['medusa'], 1) ?>%</p>
                                <div>
                                    <?= Html::a('Comprar', ['/entradas/ent'], ['class' => 'btn btn-warning', 'disabled' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }  ?>
            </div>
        </div>
    </div>
</div>