<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cantan;

/**
 * CantanSearch represents the model behind the search form of `app\models\Cantan`.
 */
class CantanSearch extends Cantan
{
    /**
     * {@inheritdoc}
     */

    public $Nombre_Cantantes;

    public function rules()
    {
        return [
            [['cod_cantantes', 'cod_conciertos'], 'integer'],
            [['Nombre_Cantantes'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cantan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinwith('codCantantes');

        // grid filtering conditions
        $query->andFilterWhere([
            'cod_cantantes' => $this->cod_cantantes,
            'cod_conciertos' => $this->cod_conciertos,
            'nombre' => $this->Nombre_Cantantes,
        ]);

        return $dataProvider;
    }
}
