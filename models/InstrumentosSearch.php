<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Instrumentos;

/**
 * InstrumentosSearch represents the model behind the search form of `app\models\Instrumentos`.
 */
class InstrumentosSearch extends Instrumentos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_conciertos'], 'integer'],
            [['instrumentos'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Instrumentos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cod_conciertos' => $this->cod_conciertos,
        ]);

        $query->andFilterWhere(['like', 'instrumentos', $this->instrumentos]);

        return $dataProvider;
    }
}
