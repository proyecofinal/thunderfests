<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Festivales;

/**
 * FestivalesSearch represents the model behind the search form of `app\models\Festivales`.
 */
class FestivalesSearch extends Festivales
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'integer'],
            [['nombre', 'fecha_inicio', 'fecha_fin'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Festivales::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cod' => $this->cod,
        ]);

        $query->andFilterWhere(['>=', 'fecha_inicio', $this->fecha_inicio])
        ->andFilterWhere(['<=', 'fecha_fin', $this->fecha_fin])
        ->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
