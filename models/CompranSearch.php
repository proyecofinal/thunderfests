<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Compran;

/**
 * CompranSearch represents the model behind the search form of `app\models\Compran`.
 */
class CompranSearch extends Compran
{
    /**
     * {@inheritdoc}
     */
    public $Nombre_Cliente;
    public $Numero_Entrada;
    public $Nombre_Festivales;
    public function rules()
    {
        return [
            [['cod_clientes', 'cod_entradas', 'cod_festivales'], 'integer'],
            [['Nombre_Cliente', 'Numero_Entrada', 'Nombre_Festivales'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Compran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinwith('codFestivales');
        
        // grid filtering conditions
        $query->andFilterWhere([
            'nombre' => $this->Nombre_Festivales,
            
        ]);

        return $dataProvider;
    }
}
