<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Código Verificación',
            'name' => 'Nombre',
            'email' => 'Correo',
            'subject' => 'Asunto',
            'body' => 'Mensaje',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        $content = "<p> Email:  $this->email  </p>
         <p> Nombre:  $this->name  </p>
         <p> Asunto:  $this->subject  </p>
         <p> Mensaje:   $this->body  </p>
         <div style='background-color: whitesmoke' dir='ltr'>
        <table style='color:rgb(0,0,0);font-family:Times New Roman; font-size:medium'>
            <tbody>
                <tr>
                    <th style='font-family:Arial,sans-serif;line-height:18px'>
                        <ul style='list-style-type:none;text-align:left'>
                            <li style='margin-top:15px;margin-bottom:5px;color:#FA9017;font-size:22px'>Thunderfests
                            </li>
                            <li style='margin-bottom:20px;font-weight:200'></li>
                            <li style='font-weight:200;font-size:14px'><strong style='color:#FA9017'>Teléfono/Whatsapp:</strong>&nbsp;(+34) 692270815 </li>
                            <li style='font-weight:200;font-size:14px'><strong style='color:#FA9017'>E:</strong>&nbsp;<a href='mailto:sceagomez@ceinmark.net' style='color:rgb(51,51,51);text-decoration:none' target='_blank'>sceagomez@ceinmark.net</a></li>
                        </ul>
                    </th>
                </tr>
            </tbody>
        </table>
        <div style='font-family: Times New Roman;font-size:10px;color:rgb(149,149,149)'>
            <p><img src='https://ci6.googleusercontent.com/proxy/LN-IyDlim-0Tb8E3NZe2s-z-qEEDuJ8BRZ8fIzaOU6IzBAAkRqBGnNdL_qmpKrigNcMWByyWZDgLv_Kb5fv7BgdKtqzGfOvnII4F7lL5Kwx7IEmfyE0NWbPu7bb4SwiXhXd3YdpW5W8qCQUu4nJ4UPOZO7KikIRNeXtOGQ=s0-d-e1-ft#http://carlos-herrera.com/wp-content/uploads/2016/02/Captura-de-pantalla-2016-02-23-a-las-13.17.43.jpg'>Antes
                de imprimir este mensaje, asegúrate de que es necesario. Proteger el medio ambiente está en tus manos.
            </p>
            <p>La información contenida en este mensaje de correo electrónico es confidencial y puede revestir el
                carácter de reservada. Está destinada exclusivamente a su destinatario. El acceso o uso de este mensaje,
                por parte de cualquier otra persona que no esté autorizada, puede ser ilegal. Si no es Ud. la persona
                destinataria, le rogamos que proceda a eliminar su contenido y comunicar dicha anomalía a su remitente.
            </p>
            <p>
                Responsable: Identidad: Sergio Cea - NIF: 722207848T Dir. postal:Calle Vargar Nº
                CP39007 Santander Teléfono: 692270815 Correo elect: sceagomez@ceinmark.net
            </p>
            <p>En nombre de la empresa tratamos la información que nos facilita con el fin de enviarle información
                relacionada con nuestros productos y servicios por cualquier medio (postal, email o teléfono). Los datos
                proporcionados se conservarán mientras no solicite el cese de la actividad. Los datos no se cederán a
                terceros salvo en los casos en que exista una obligación legal.</p>
            <p> Usted tiene derecho a obtener confirmación sobre si en Thunderfests estamos tratando sus
                datos personales por tanto tiene derecho a acceder a sus datos personales, rectificar los datos inexacto
                o solicitar su supresión cuando los datos ya no sean necesarios para los fines que fueron recogidos</p>
        </div>
    </div>
         ";
        if ($this->validate()) {
            Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
