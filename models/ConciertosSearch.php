<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Conciertos;

/**
 * ConciertosSearch represents the model behind the search form of `app\models\Conciertos`.
 */
class ConciertosSearch extends Conciertos
{
    /**
     * {@inheritdoc}
     */

    public $Nombre_Festivales;

    public function rules()
    {
        return [
            [['cod', 'cod_festivales'], 'integer'],
            [['fecha', 'hora_inicio', 'hora_fin', 'Nombre_Festivales'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Conciertos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinwith('codFestivales');

        // grid filtering conditions
        $query->andFilterWhere([
            'cod' => $this->cod,
            'fecha' => $this->fecha,
            'hora_inicio' => $this->hora_inicio,
            'hora_fin' => $this->hora_fin,
            'cod_festivales' => $this->cod_festivales,
            'nombre' => $this->Nombre_Festivales,

        ]);

        return $dataProvider;
    }
}
