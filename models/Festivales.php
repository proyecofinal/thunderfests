<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "festivales".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 *
 * @property Compran[] $comprans
 * @property Clientes[] $codClientes
 * @property Entradas[] $codEntradas
 * @property Conciertos[] $conciertos
 * @property Entradas[] $entradas
 */
class Festivales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'festivales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha_inicio', 'fecha_fin'], 'required'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['nombre'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre del Festival',
            'fecha_inicio' => 'Fecha de Inicio',
            'fecha_fin' => 'Fecha de Fin',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[CodClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodClientes()
    {
        return $this->hasMany(Clientes::className(), ['cod' => 'cod_clientes'])->viaTable('compran', ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[CodEntradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntradas()
    {
        return $this->hasMany(Entradas::className(), ['cod' => 'cod_entradas'])->viaTable('compran', ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[Conciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConciertos()
    {
        return $this->hasMany(Conciertos::className(), ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['cod_festivales' => 'cod']);
    }
}
