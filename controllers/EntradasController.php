<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Entradas;
use app\models\EntradasSearch;
use kartik\mpdf\Pdf;




/**
 * EntradasController implements the CRUD actions for Entradas model.
 */
class EntradasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entradas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EntradasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    /**
     * Displays a single Entradas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Entradas();
        $nombre = "Crear Entrada";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('create', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Updates an existing Entradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $nombre = "Actualizar";
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('update', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Deletes an existing Entradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionEnt()
    {
        $aforo = array(
            "medusa" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '109'])->count('numero_entrada'),
            "riverland" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '9'])->count('numero_entrada'),
            "sunblast" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '533'])->count('numero_entrada'),
            "arenal" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '1000'])->count('numero_entrada'),
            
        );

        $ventas = array(
            "medusa" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '109'])->count('cod_entradas') * 100,
            "riverland" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '9'])->count('cod_entradas') * 100,
            "sunblast" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '533'])->count('cod_entradas') * 100,
            "arenal" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '1000'])->count('cod_entradas') * 100,
            
        );

        $procentajes = array(
            "medusa" => $ventas['medusa']/$aforo['medusa'],
            "riverland" => $ventas['riverland']/$aforo['riverland'],
            "sunblast" => $ventas['sunblast']/$aforo['sunblast'],
            "arenal" => $ventas['arenal']/$aforo['arenal'],
            
            
        );
        return $this->render('entradas', 
        [
            'aforo' => $aforo,
            'procentaje' => $procentajes,    
        ]);
    }

    public function actionFilter()
    {
        $model = new EntradasSearch();
        return $this->render('_search', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Entradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entradas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    

    public function actionImprimir (){
        // Puedes vaciar todo el contenido html en una variable
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(),
        ]);

        $content = $this->renderPartial('/layouts/datos_entradas',
        [
          'dataProvider'=>$dataProvider,        
          
          ]); 

        // Debes crear un objeto el cual vas a usar para generar el pdf.
        // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            //'content' => $this->renderPartial('privacy'),
            
            'destination' => Pdf::DEST_DOWNLOAD,
            
                // portrait orientation
                //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        ]);

        $file_name = 'Datos_Entradas.pdf';                                                            

        $pdf->content = $content;
        $pdf->filename = $file_name;
        //echo "Descomenta para ver html:" . $content;

        return $pdf->render();          
    }

}


