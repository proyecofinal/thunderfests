<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Festivales;
use app\models\Conciertos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $aforo = array(
            "medusa" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '109'])->count('numero_entrada'),
            "riverland" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '9'])->count('numero_entrada'),
            "sunblast" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '533'])->count('numero_entrada'),
            "arenal" => (new \yii\db\Query())->from('entradas')->where(['cod_festivales' => '1000'])->count('numero_entrada'),
            
        );

        $clientes = (new \yii\db\Query())->from('clientes')->count('cod');
        $compras = (new \yii\db\Query())->from('compran')->count('cod_clientes');
        $festivales = (new \yii\db\Query())->from('festivales')->count('cod');

        $ventas = array(
            "medusa" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '109'])->count('cod_entradas') * 100,
            "riverland" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '9'])->count('cod_entradas') * 100,
            "sunblast" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '533'])->count('cod_entradas') * 100,
            "arenal" => (new \yii\db\Query())->from('compran')->where(['cod_festivales' => '1000'])->count('cod_entradas') * 100,
            
        );

        $procentajes = array(
            "medusa" => $ventas['medusa']/$aforo['medusa'],
            "riverland" => $ventas['riverland']/$aforo['riverland'],
            "sunblast" => $ventas['sunblast']/$aforo['sunblast'],
            "arenal" => $ventas['arenal']/$aforo['arenal'],
            
            
        );

        $events = Festivales::find()->all();
        $tasks = [];
        
        foreach ($events as $eve) {
            $event = new \yii2fullcalendar\models\Event();
            $event->id = $eve->cod;
            $event->backgroundColor = '#222222';
            $event->title = $eve->nombre;
            $event->start = $eve->fecha_inicio;
            $tasks[] = $event;
            
        }

        
        
        return $this->render('index', [
            'aforo' => $aforo,
            'clientes' => $clientes,    
            'compras' => $compras,    
            'festivales' => $festivales,    
            'procentaje' => $procentajes,  
            'events' => $tasks,  
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
}
