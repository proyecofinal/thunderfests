<?php

namespace app\controllers;

use Yii;
use app\models\Cantan;
use app\models\CantanSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * CantanController implements the CRUD actions for Cantan model.
 */
class CantanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cantan models.
     * @return mixed
     */
    public function actionIndex()
    {
       $dataProvider = new ActiveDataProvider([
           'query' => Cantan::find(),
       ]);
       $searchModel = new CantanSearch();
       $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       return $this->render('index', [
           'searchModel' => $searchModel, 
           'dataProvider' => $dataProvider,
       ]);
    }
    
    public function actionFilter()
    {
        $model = new CantanSearch();
        return $this->render('_search', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Cantan model.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_cantantes, $cod_conciertos)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_cantantes, $cod_conciertos),
        ]);
    }

    /**
     * Creates a new Cantan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $nombre = "Añadir Cantan";
        $model = new Cantan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]);
        }

        return $this->render('create', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Updates an existing Cantan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_cantantes, $cod_conciertos)
    {
        $nombre = "Actualizar Cantan";
        $model = $this->findModel($cod_cantantes, $cod_conciertos);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]);
        }

        return $this->render('update', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Deletes an existing Cantan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_cantantes, $cod_conciertos)
    {
        $this->findModel($cod_cantantes, $cod_conciertos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cantan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return Cantan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_cantantes, $cod_conciertos)
    {
        if (($model = Cantan::findOne(['cod_cantantes' => $cod_cantantes, 'cod_conciertos' => $cod_conciertos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDatos (){
        // Puedes vaciar todo el contenido html en una variable
        $dataProvider = new ActiveDataProvider([
            'query' => Cantan::find(),
        ]);

        $content = $this->renderPartial('/layouts/datos_cantan',
        [
          'dataProvider'=>$dataProvider,        
          
          ]); 

        // Debes crear un objeto el cual vas a usar para generar el pdf.
        // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            //'content' => $this->renderPartial('privacy'),
            
            'destination' => Pdf::DEST_DOWNLOAD,
            
                // portrait orientation
                //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        ]);

        $file_name = 'Datos_Cantan.pdf';                                                            

        $pdf->content = $content;
        $pdf->filename = $file_name;
        //echo "Descomenta para ver html:" . $content;

        return $pdf->render();          
    }
}
