<?php

namespace app\controllers;

use Yii;
use app\models\Compran;
use app\models\Entradas;
use app\models\Clientes;
use app\models\CompranSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;


/**
 * CompranController implements the CRUD actions for Compran model.
 */
class CompranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Compran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Compran::find(),
        ]);
        $searchModel = new CompranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compran model.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_clientes, $cod_entradas, $cod_festivales)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_clientes, $cod_entradas, $cod_festivales),
        ]);
    }

    /**
     * Creates a new Compran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        $nombre  = "Comprar";
        $model = new Compran();
        
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]);
        }

        return $this->render('create', [
            'model' => $model,
            'nombre' => $nombre,
            
        ]);
    }

    /**
     * Updates an existing Compran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $nombre = "Actualizar Compra";
        $model = $this->findModel($cod_clientes, $cod_entradas, $cod_festivales);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]);
        }

        return $this->render('update', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Deletes an existing Compran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $this->findModel($cod_clientes, $cod_entradas, $cod_festivales)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return Compran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_clientes, $cod_entradas, $cod_festivales)
    {
        if (($model = Compran::findOne(['cod_clientes' => $cod_clientes, 'cod_entradas' => $cod_entradas, 'cod_festivales' => $cod_festivales])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModels($cod_clientes)
    {
        if (($model = Compran::findOne(['cod_clientes' => $cod_clientes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFilter()
    {
        $model = new CompranSearch();
        return $this->render('_search', [
            'model' => $model,
        ]);
    }

    public function actionListado ($cod_festivales) {

        $lista = Entradas::findBySql('select * from entradas where cod_festivales = "'.$cod_festivales.'" and cod not in (select distinct cod_entradas from compran)')->all();
        if (count($lista)>0) {
            echo "<option> --Selecciona una Entrada--</option>";
            foreach ($lista as $list) {
                echo '<option value = "'.$list->cod.'">'.$list->numero_entrada.'</option>';
            }
        } else {
            echo "<option>----</option>";
        }
    }

    public function actionImprimir($cod_clientes, $cod_entradas, $cod_festivales)
    {
          // Puedes vaciar todo el contenido html en una variable
          $model = $this->findModel($cod_clientes, $cod_entradas, $cod_festivales);
          $content = $this->renderPartial('/layouts/datos_entrada',
          [
            'model'=>$model,        
            
            ]); 

          // Debes crear un objeto el cual vas a usar para generar el pdf.
          // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
          $pdf = new Pdf([
              'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
              //'content' => $this->renderPartial('privacy'),
              
              'destination' => Pdf::DEST_DOWNLOAD,
              
                  // portrait orientation
                  //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                  // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
          ]);

          $file_name = 'entrada_' .$model->codFestivales->nombre. '_' .$model->codClientes->nombre.'.pdf';                                                            

          $pdf->content = $content;
          $pdf->filename = $file_name;
          //echo "Descomenta para ver html:" . $content;

          return $pdf->render(); 
    }

    public function actionNombre()
    {
        return $this->render('cambio_nombre');
        
    }
    public function actionCambio()
    {
        $clientes = $_POST["Clientes"]["dni"];
        $cod_clientes = Clientes::findBySql('select * from clientes where dni = "'.$clientes.'" ')->all();
        $model = "";
        $cliente = "";
        foreach ($cod_clientes as $c) {
            $model = $this->findModels($c->cod);
            $cliente = $c->cod;
        }

        $cod_entradas = Compran::findBySql('select * from compran where cod_clientes = "'.$cliente.'" ')->all();
        $entrada = "";
        foreach ($cod_entradas as $e) {
            $entrada = $e->cod_entradas;
        }

        $cod_festivales = Compran::findBySql('select * from compran where cod_clientes = "'.$cliente.'" ')->all();
        $festival = "";
        foreach ($cod_festivales as $f) {
            $festival = $f->cod_festivales;
        }

        return $this->render('cambio', [
            'cliente' => $cliente,
            'entrada' => $entrada,
            'festival' => $festival,
            'model' => $model,
        ]);
        
    }

    public function actionDatos (){
        // Puedes vaciar todo el contenido html en una variable
        $dataProvider = new ActiveDataProvider([
            'query' => Compran::find(),
        ]);

        $content = $this->renderPartial('/layouts/datos_compras',
        [
          'dataProvider'=>$dataProvider,        
          
          ]); 

        // Debes crear un objeto el cual vas a usar para generar el pdf.
        // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            //'content' => $this->renderPartial('privacy'),
            
            'destination' => Pdf::DEST_DOWNLOAD,
            'cssFile' => '@vendor/yiisoft/yii2-bootstrap/src/BootstrapAsset',
            
                // portrait orientation
                //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                
        ]);

        $file_name = 'Datos_Compras.pdf';                                                            

        $pdf->content = $content;
        $pdf->filename = $file_name;
        //echo "Descomenta para ver html:" . $content;

        return $pdf->render();          
    }

    public function actionActualizar($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $nombre = "Actualizar Compra";
        $model = $this->findModel($cod_clientes, $cod_entradas, $cod_festivales);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]);
        }

        return $this->render('actualizar', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }
}
