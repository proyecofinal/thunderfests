<?php

namespace app\controllers;

use Yii;
use app\models\Instrumentos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use app\models\InstrumentosSearch;
/**
 * InstrumentosController implements the CRUD actions for Instrumentos model.
 */
class InstrumentosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Instrumentos models.
     * @return mixed
     */
    public function actionIndex()
   {
       $dataProvider = new ActiveDataProvider([
           'query' => Instrumentos::find(),
       ]);
       $searchModel = new InstrumentosSearch();
       $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       return $this->render('index', [
           'searchModel' => $searchModel, 
           'dataProvider' => $dataProvider,
       ]);
    }

    public function actionFilter()
    {
        $model = new InstrumentosSearch();
        return $this->render('_search', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Instrumentos model.
     * @param integer $cod_conciertos
     * @param string $instrumentos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_conciertos, $instrumentos)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_conciertos, $instrumentos),
        ]);
    }

    /**
     * Creates a new Instrumentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $nombre = "Añadir Instrumento";
        $model = new Instrumentos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos]);
        }

        return $this->render('create', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Updates an existing Instrumentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cod_conciertos
     * @param string $instrumentos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_conciertos, $instrumentos)
    {
        $nombre = "Actualizar Instrumento";
        $model = $this->findModel($cod_conciertos, $instrumentos);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos]);
        }

        return $this->render('update', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Deletes an existing Instrumentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cod_conciertos
     * @param string $instrumentos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_conciertos, $instrumentos)
    {
        $this->findModel($cod_conciertos, $instrumentos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrumentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cod_conciertos
     * @param string $instrumentos
     * @return Instrumentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_conciertos, $instrumentos)
    {
        if (($model = Instrumentos::findOne(['cod_conciertos' => $cod_conciertos, 'instrumentos' => $instrumentos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDatos (){
        // Puedes vaciar todo el contenido html en una variable
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find(),
        ]);

        $content = $this->renderPartial('/layouts/datos_instrumentos',
        [
          'dataProvider'=>$dataProvider,        
          
          ]); 

        // Debes crear un objeto el cual vas a usar para generar el pdf.
        // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            //'content' => $this->renderPartial('privacy'),
            
            'destination' => Pdf::DEST_DOWNLOAD,
            
                // portrait orientation
                //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        ]);

        $file_name = 'Datos_Instrumentos.pdf';                                                            

        $pdf->content = $content;
        $pdf->filename = $file_name;
        //echo "Descomenta para ver html:" . $content;

        return $pdf->render();          
    }
}
