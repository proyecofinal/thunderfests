<?php

namespace app\controllers;

use Yii;
use app\models\Conciertos;
use app\models\ConciertosSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * ConciertosController implements the CRUD actions for Conciertos model.
 */
class ConciertosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Conciertos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conciertos::find(),
        ]);
        $searchModel = new ConciertosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFilter()
    {
        $model = new ConciertosSearch();
        return $this->render('_search', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Conciertos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Conciertos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $nombre = "Añadir Concierto";
        $model = new Conciertos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('create', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Updates an existing Conciertos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $nombre = "Actualizar Concierto";
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cod]);
        }

        return $this->render('update', [
            'model' => $model,
            'nombre' => $nombre,
        ]);
    }

    /**
     * Deletes an existing Conciertos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conciertos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Conciertos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Conciertos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDatos (){
        // Puedes vaciar todo el contenido html en una variable
        $dataProvider = new ActiveDataProvider([
            'query' => Conciertos::find(),
        ]);

        $content = $this->renderPartial('/layouts/datos_conciertos',
        [
          'dataProvider'=>$dataProvider,        
          
          ]); 

        // Debes crear un objeto el cual vas a usar para generar el pdf.
        // El parámetro 'content' puede ser sustituido con una vista, yo te recomiendo usar la función: $this->renderPartial('_ruta_de_tu_vista')
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            //'content' => $this->renderPartial('privacy'),
            
            'destination' => Pdf::DEST_DOWNLOAD,
            
                // portrait orientation
                //'orientation' => Pdf::ORIENT_LANDSCAPE,        
                // 'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        ]);

        $file_name = 'Datos_Conciertos.pdf';                                                            

        $pdf->content = $content;
        $pdf->filename = $file_name;
        //echo "Descomenta para ver html:" . $content;

        return $pdf->render();          
    }
}
